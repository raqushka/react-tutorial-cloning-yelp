import React, { PropTypes } from 'react'
import { Router } from 'react-router'

class App extends React.Component {
  static contextTypes = {
    router: PropTypes.object
  }
  
  static PropTypes = {
    routes: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  }

  get content() {
    return (
      <Router
        routes={this.props.routes}
        history={this.props.history}
      />
    )
  }

  render() {
    return (
      <div style={{height: '100%'}}>
        {this.content}
      </div>
    )
  }

}

module.exports = App;
